import requests
 
from lxml import html
from bs4 import BeautifulSoup
import os
import json
import time
import datetime
import urllib.request
from datetime import datetime
 
# All offers are saved in Json file, so at the beginning I load everything into variable, to check for URLs. I treat URLs as unique identifiers
 

 
# This function will open n-th page and save all details of offers that are not present in tiguan.json

def telegram_bot_sendtext(bot_chatID,bot_message):

   bot_token = '1889906923:AAGC0Zfq5guawXAeJ1tAYZX1LihpgdOwCVA'
   send_text = 'https://api.telegram.org/bot' + bot_token + '/sendMessage?chat_id=' + bot_chatID + '&parse_mode=Markdown&text=' + bot_message

   response = requests.get(send_text)

   return response.json()

 
def get_offers(n,url):
 
#This URL is what I was looking for, but you may change it as you need
    global previous_offers
    url = url +'&page='+str(n)
 
    headers = {'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36'}
 
    request = requests.get(url, headers = headers)
 
    tree = html.fromstring(request.text)
 
    xpath_offer_details = '//div[@class="offers list"]/article'#//text()
 
    xpath_url = '//div[@class="offers list"]/article/@data-href'#//text()
 
    offer_details = tree.xpath(xpath_offer_details)
 
    list_of_urls = tree.xpath(xpath_url)
    
    for item in list_of_urls:
        if item not in previous_offers:
            previous_offers.append(item)
            get_single_offer(item)



def get_single_offer(html_element):
    
    res = requests.get(html_element)
    if res.status_code == 200:
        soup = BeautifulSoup(res.text, 'html.parser')
        main_details = soup.find_all(class_="offer-main-params__item")
        name = soup.find_all(class_="offer-title big-text fake-title")
        name = name[0].text
        name = name.replace("  ", "")
        name = name.replace("\n", "")
        year = main_details[0].text
        year = year.replace(" ", "")
        year = year.replace("\n", "")
        engine_size = soup.findAll(text='Pojemność skokowa')
        engine_size = engine_size[0].next_element.next_sibling.text
        engine_size = engine_size.replace("  ", "")
        engine_size = engine_size.replace("\n", "")
        horse_power = soup.findAll(text='Moc')
        horse_power = horse_power[0].next_element.next_sibling.text
        horse_power = horse_power.replace("  ", "")
        horse_power = horse_power.replace("\n", "")
        km = main_details[1].text
        km = km.replace("  ", "")
        km = km.replace("\n", "")
        km = km.replace("km", " km")
        engine = main_details[2].text
        engine = engine.replace("  ", "")
        engine = engine.replace("\n", "")
        type = main_details[3].text
        type = type.replace("  ", "")
        type = type.replace("\n", "")
        price = soup.find_all(class_="offer-price")
        price = price[0].attrs['data-price']
        currency = soup.find_all(class_="offer-price__currency")
        currency = currency[0].text
        localization = soup.find_all(class_="seller-card__links__link__cta")
        localization = localization[0].text
        localization = localization.replace("  ", "")
        localization = localization.replace("\n", "")

        telegram_bot_sendtext("1780565004","Nazwa: "+ name + "\nRok produkcji: " +year + "\nPrzebieg: " + km + "\nPojemność skokowa: " + engine_size + "\nMoc: " + horse_power + "\nRodzaj paliwa: " + engine + "\nTyp nadwozia: " + type + "\nCena: " + price + " " + currency + "\nLokalizacja: " + localization + "\n\n" + html_element)
        telegram_bot_sendtext("1844685832","Nazwa: "+ name + "\nRok produkcji: " +year + "\nPrzebieg: " + km + "\nPojemność skokowa: " + engine_size + "\nMoc: " + horse_power + "\nRodzaj paliwa: " + engine + "\nTyp nadwozia: " + type + "\nCena: " + price + " " + currency + "\nLokalizacja: " + localization + "\n\n" + html_element)


def get_number_of_pages(url):
    url = url +'&page=1'
    res = requests.get(url)
    if res.status_code == 200:
        soup = BeautifulSoup(res.text, 'html.parser')
        is_one_page = soup.find_all(class_="om-pager")
        no_cars = soup.find_all(class_="criteriaChangeWarning")
        if no_cars != []:
            return int(0)
        if is_one_page == []:
            return int(1)
        headers = {'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36'}
        request = requests.get(url, headers = headers)
        tree = html.fromstring(request.text)
        max_page= tree.xpath('//ul[@class="om-pager rel"]/li[last()-1]/a/span/text()')[0].strip()
        return int(max_page)

starttime = time.time()
while True:
    with open('auta.json') as json_file:
        previous_offers = json.load(json_file)
    with open('urls.json') as url_file:
        url = json.load(url_file)
    for item in url:    
        max_number_of_pages = get_number_of_pages(item)
        if isinstance(max_number_of_pages,int):
            for i in range (1,max_number_of_pages+1):
                get_offers(i, item)
    with open('auta.json', 'w', encoding='utf-8') as f:
        json.dump(previous_offers, f, ensure_ascii=False, indent=4)
    now = datetime.now()
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    print("date and time =", dt_string)
    time.sleep(60.0 - ((time.time() - starttime) % 60.0))
