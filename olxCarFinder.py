import requests
 
from lxml import html
from bs4 import BeautifulSoup
import os
import json
import time
import datetime
import urllib.request
from datetime import datetime

def olx_get_offers(n,url):
 
#This URL is what I was looking for, but you may change it as you need
    global previous_offers
    url = url +'&page='+str(n)
    headers = {'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36'}
    request = requests.get(url, headers = headers)
    tree = html.fromstring(request.text)
 
    xpath_url = '//table[@summary="Ogłoszenia"]/tbody/tr[@class="wrap"]/td/div[@class="offer-wrapper"]/table/tbody/tr/td[@valign="top"]/div/h3/a/@href'#//text()
 
    list_of_urls = tree.xpath(xpath_url)

    return 0
    
    # for item in list_of_urls:
    #     if item not in previous_offers:
    #         previous_offers.append(item)
    #         get_single_offer(item)
 
def olx_get_number_of_pages(url):
    url = url +'&page=1'
    res = requests.get(url)
    if res.status_code == 200:
        soup = BeautifulSoup(res.text, 'html.parser')
        is_one_page = soup.find_all(class_="pager rel clr")
        no_cars = soup.find_all(class_="c41 lheight24")
        if no_cars != []:
            return int(0)
        if is_one_page == []:
            return int(1)
        else:
            last_page_value = soup.find('a',{'data-cy': 'page-link-last'})
            last_page_value = last_page_value.text
            last_page_value = last_page_value.replace("\n", "")
            return int(last_page_value)
            # headers = {'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36'}
            # request = requests.get(url, headers = headers)
            # tree = html.fromstring(request.text)
            # max_page= tree.xpath('//ul[@class="pager rel clr"]/li[last()-1]/a/span/text()')[0].strip()
            # return int(max_page)

url = "https://www.olx.pl/motoryzacja/samochody/kia/ceed/?search%5Bfilter_float_price%3Ato%5D=36000&search%5Bfilter_float_price%3Afrom%5D=20000&search%5Bfilter_float_year%3Afrom%5D=2009&search%5Bfilter_float_enginesize%3Afrom%5D=1300&search%5Bfilter_float_milage%3Ato%5D=150000&search%5Bfilter_enum_country_origin%5D%5B0%5D=pl"
url2 = "https://www.olx.pl/motoryzacja/samochody/kia/ceed/walcz/?search%5Bfilter_float_price%3Afrom%5D=20000&search%5Bfilter_float_price%3Ato%5D=36000&search%5Bfilter_float_year%3Afrom%5D=2009&search%5Bfilter_float_enginesize%3Afrom%5D=1300&search%5Bfilter_float_milage%3Ato%5D=150000&search%5Bfilter_enum_country_origin%5D%5B0%5D=pl&search%5Bdist%5D=100"
url3 = "https://www.olx.pl/motoryzacja/samochody/kia/ceed/walcz/?search%5Bfilter_float_price%3Afrom%5D=20000&search%5Bfilter_float_price%3Ato%5D=36000&search%5Bfilter_float_year%3Afrom%5D=2009&search%5Bfilter_float_enginesize%3Afrom%5D=1300&search%5Bfilter_float_milage%3Ato%5D=150000&search%5Bfilter_enum_country_origin%5D%5B0%5D=pl"

n = olx_get_number_of_pages(url)
olx_get_offers(n,url)